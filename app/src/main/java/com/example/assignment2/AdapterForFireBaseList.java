package com.example.assignment2;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterForFireBaseList extends ArrayAdapter<Coins> {

    private Activity context;
    private List<Coins> coinsList;

    public AdapterForFireBaseList(Activity context, List<Coins> coinsList)
    {
        super(context, R.layout.list_layout,coinsList);
        this.context = context;
        this.coinsList = coinsList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.list_layout,null,true);
        TextView name =(TextView) listViewItem.findViewById(R.id.textViewCrypto) ;
        TextView price= (TextView) listViewItem.findViewById(R.id.textViewPrice) ;

        Coins coins = coinsList.get(position);

        name.setText(coins.getCrypto());
        price.setText(coins.getPrice());


        return listViewItem;
    }
}
