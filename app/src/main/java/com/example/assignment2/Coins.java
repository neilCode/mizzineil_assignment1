package com.example.assignment2;

public class Coins {
    String crypto;
    String price;

    public Coins()
    {

    }

    public Coins(String crypto, String price) {
        this.crypto = crypto;
        this.price = price;
    }

    public String getCrypto() {
        return crypto;
    }

    public String getPrice() {
        return price;
    }
}
