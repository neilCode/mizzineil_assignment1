package com.example.assignment2;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class CryptoClientForRecycle {

    private static String APP_ID = "a6fe4238438b53ca3be30c5123a671c06b5246f08e442880d89b840e7b045c1e";

    public String getData(String url)
    {
        HttpsURLConnection con = null;
        InputStream is = null;

        try {
            con = (HttpsURLConnection) ( new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoOutput(false);
            con.addRequestProperty("Apikey",APP_ID);
            con.connect();
            Log.wtf("testn",con.getResponseMessage());
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();

            return buffer.toString();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;
    }
}
