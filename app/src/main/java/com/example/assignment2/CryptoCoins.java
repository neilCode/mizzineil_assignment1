package com.example.assignment2;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

public class CryptoCoins extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crypto_coins);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    public void getValues(View v){

        TextView value1 = findViewById(R.id.btc);
        TextView value2= findViewById(R.id.eth);
        TextView value3 = findViewById(R.id.euro);
        TextView value4 = findViewById(R.id.usd);

        CryptoTask task1 = new CryptoTask();
        CryptoTask task2 = new CryptoTask();
        CryptoTask task3 = new CryptoTask();
        CryptoTask task4 = new CryptoTask();

       String x = value1.getText().toString();

        try {
            double temp = task1.execute(value1.getText().toString(),value3.getText().toString()).get();
            double temp1 = task2.execute(value1.getText().toString(),value4.getText().toString()).get();
            double temp2 = task3.execute(value2.getText().toString(),value3.getText().toString()).get();
            double temp3 = task4.execute(value2.getText().toString(),value4.getText().toString()).get();

            TextView txtTemp = findViewById(R.id.btc_euro);
            txtTemp.setText(String.valueOf(temp));

            TextView txtTemp1 = findViewById(R.id.btc_usd);
            txtTemp1.setText(String.valueOf(temp1));


            TextView txtTemp2 = findViewById(R.id.eth_euro);
            txtTemp2.setText(String.valueOf(temp2));

            TextView txtTemp3 = findViewById(R.id.eth_usd);
            txtTemp3.setText(String.valueOf(temp3));

        }
        catch(EnumConstantNotPresentException e)
        {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
}
