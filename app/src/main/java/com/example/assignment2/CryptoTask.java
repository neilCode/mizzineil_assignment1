package com.example.assignment2;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

public class CryptoTask extends AsyncTask<String, Void, Double> {

    @Override
    protected Double doInBackground(String... locations) {
        double temp = 0;
        CryptoClient client = new CryptoClient();

        String resultJSON = client.getData(locations[0],locations[1]);

        JSONObject jObj = null;
        try {
            jObj = new JSONObject(resultJSON);
            JSONObject A= jObj.getJSONObject(locations[0]);
            double valueR= A.getDouble(locations[1]);
            return valueR;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return temp;
    }
}

