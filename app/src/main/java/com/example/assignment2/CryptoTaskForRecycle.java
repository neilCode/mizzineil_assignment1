package com.example.assignment2;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class CryptoTaskForRecycle extends AsyncTask<String, Void, List<ListItem>> {

    @Override
    protected List<ListItem> doInBackground(String... locations) {
        String[] currencies = {"BTC","ETH","XRP","EOS","LTC","BCH","NEO","XLM","TRX"};
        List<ListItem> n =new ArrayList<>();

        CryptoClientForRecycle client = new CryptoClientForRecycle();

        String resultJSON = client.getData(locations[0]);

        JSONObject jObj = null;

        try {

            jObj = new JSONObject(resultJSON);
            for(int i=0;i<jObj.length();i++)
            {
                n.add(new ListItem(currencies[i],jObj.getJSONObject(currencies[i]).getString("EUR")));
            }
            return n;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

