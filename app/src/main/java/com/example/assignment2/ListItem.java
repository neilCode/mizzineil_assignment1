package com.example.assignment2;

public class ListItem {

    private String head;
    private String price;


    public ListItem(String head, String price) {
        this.head = head;
        this.price = price;
    }

    public String getHead() {
        return head;
    }

    public String getPrice() {
        return price;
    }

}
