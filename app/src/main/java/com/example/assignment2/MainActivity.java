package com.example.assignment2;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class MainActivity extends AppCompatActivity {

    public void openActivity() {
        Intent intent = new Intent(this, Navbar.class);
        startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Pinview pinview = (Pinview)findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                if (pinview.getValue().equals("1234")) {
                    Toast.makeText(MainActivity.this, "Correct Password" + pinview.getValue(), Toast.LENGTH_SHORT).show();
                    openActivity();
                }
                else{
                    Toast.makeText(MainActivity.this, "Incorrect Password" + pinview.getValue(), Toast.LENGTH_SHORT).show();
                    for (int i = 0;i < pinview.getPinLength();i++) {
                        pinview.onKey(pinview.getFocusedChild(), KeyEvent.KEYCODE_DEL, new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_DEL));
                    }
                }
            }
        });
    }
}
