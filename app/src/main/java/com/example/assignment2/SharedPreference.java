package com.example.assignment2;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class SharedPreference extends AppCompatActivity {


    EditText currency;
    EditText price;
    TextView showEntered;
    DatabaseReference databaseCr;

    ListView listView;
    List<Coins> coinsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        databaseCr = FirebaseDatabase.getInstance().getReference("Crypto");

        currency =(EditText) findViewById(R.id.currencyInput);
        price = (EditText) findViewById(R.id.priceInput);
        showEntered= (TextView)findViewById(R.id.showResult);

        listView = (ListView) findViewById(R.id.listViewCoins);

        coinsList = new ArrayList<>();

    }

    public void saveDataToFirebase(View view)
    {
        String currencyValue = currency.getText().toString().trim();
        String priceValue = price.getText().toString().trim();

        if (!TextUtils.isEmpty(currencyValue) && !TextUtils.isEmpty(priceValue))
        {
            String id =databaseCr.push().getKey();
            Coins coins = new Coins(currencyValue,priceValue);
            databaseCr.child(id).setValue(coins);

            Toast.makeText(this,"Coin Added",Toast.LENGTH_LONG).show();
        }
        else
            {
                Toast.makeText(this,"Enter a value",Toast.LENGTH_LONG).show();
            }
    }
    public void saveDate() {

        Date time = Calendar.getInstance().getTime();

        SharedPreferences sharedPreferences = getSharedPreferences("userInfo",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("date",time.toString());

        Toast.makeText(this,"Save Date",Toast.LENGTH_LONG).show();
    }

    public void displayData()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("userInfo",Context.MODE_PRIVATE);

        String cur = sharedPreferences.getString("currency","");
        String pri = sharedPreferences.getString("price","");
        showEntered.setText(cur+""+pri);
    }

    public void displayDataSnackBar()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("userInfo",Context.MODE_PRIVATE);
        String date = sharedPreferences.getString("date","");

        Log.i(date,"Check");
        View parentLayout = findViewById(R.id.fab);
        Snackbar.make(parentLayout, date, Snackbar.LENGTH_LONG).setAction("CLOSE", null).show();


    }

    public void saveInfo()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("userInfo",Context.MODE_PRIVATE);

        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("currency",currency.getText().toString());
        editor.putString("price",price.getText().toString());

        editor.apply();

        Toast.makeText(this,"Save",Toast.LENGTH_LONG).show();
    }

    private  static final String TAG = "SharedPreference";

    @Override
    protected void onStart()
    {
        super.onStart();
        displayData();
        displayDataSnackBar();
        databaseCr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Anything changed in database
                coinsList.clear();
                for (DataSnapshot snapshot:dataSnapshot.getChildren())
                {
                    //Refreshing every time


                    Coins coins = snapshot.getValue(Coins.class);
                    coinsList.add(coins);
                }

                AdapterForFireBaseList adapter = new AdapterForFireBaseList(SharedPreference.this,coinsList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //if there is error
            }
        });
        Log.i(TAG,"onStart");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        displayData();
        displayDataSnackBar();
        Log.i(TAG, "onResume");
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        saveInfo();
        Log.i(TAG, "onPause");
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        saveInfo();
        Log.i(TAG, "onStop");
    }

}
