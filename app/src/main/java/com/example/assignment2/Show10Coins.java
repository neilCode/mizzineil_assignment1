package com.example.assignment2;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.ArrayList;

public class Show10Coins extends AppCompatActivity {

    public ArrayList<String> getNames() {
        ArrayList<String> cList = new ArrayList<String>();
        String[] items = {"BTC", "ETH", "XRP", "EOS", "LTC", "BCH", "NEO", "XLM", "TRX"};
        for (int i = 0; i < items.length; i++) {
            cList.add(items[i]);
        }
        return  cList;
    }


        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show10_coins);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        ArrayList<String> items= getNames();
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.spinnerlayout,R.id.txt,items);
        spinner.setAdapter(adapter);
    }

}
